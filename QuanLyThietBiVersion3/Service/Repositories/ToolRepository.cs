﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service.Repositories
{
    public interface IToolRepository
    {
        IEnumerable<Tool> GetTools();

        IEnumerable<Tool> GetToolsByCategoryId(int id);
        Tool GetToolByID(int toolId);

        void Add(Tool Tool);
        void Delete(int ToolID);
        void Update(Tool Tool);
        void Save();
    }
    public class ToolRepository : IToolRepository
    {
        private QuanLyThietBiContext context;

        public ToolRepository(QuanLyThietBiContext _context)
        {
            this.context = _context;
        }

        public void Add(Tool Tool)
        {
            context.Tools.Add(Tool);
        }

        public void Delete(int toolID)
        {
            Tool tool = context.Tools.Find(toolID);
            context.Tools.Remove(tool);
        }
        public IEnumerable<Tool> GetTools()
        {
            return context.Tools.ToList();
        }

        public Tool GetToolByID(int ToolId)
        {
            return context.Tools.Find(ToolId);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(Tool tool)
        {
            context.Entry(tool).State = EntityState.Modified;
        }

        public IEnumerable<Tool> GetToolsByCategoryId(int id)
        {
            var cate_tool = context.CategoryTools.Where(x => x.CategoryToolId == id);
            var listTool = context.Tools.Where(x => x.CategoryTool == cate_tool);
            return listTool;
        }

        public IEnumerable<Tool> GetToolsByCategoryName (string categoryToolName)
        {
            var cate_tool = context.CategoryTools.Where(x => x.CategoryToolName.Contains(categoryToolName));
            var listTool = context.Tools.Where(x => x.CategoryTool == cate_tool);
            return listTool;
        }
    }
}
