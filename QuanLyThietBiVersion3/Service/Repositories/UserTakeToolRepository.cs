﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service.Repositories
{
    public interface IUserTakeToolRepository
    {
        IEnumerable<UserTakeTool> GetUserTakeTools();
        UserTakeTool GetUserTakeToolByID(int UserTakeToolId);
        void Add(UserTakeTool UserTakeTool);
        void Delete(int UserTakeToolID);
        void Update(UserTakeTool UserTakeTool);
        void Save();
    }
    public class UserTakeToolRepository : IUserTakeToolRepository
    {
        private QuanLyThietBiContext context;

        public UserTakeToolRepository(QuanLyThietBiContext _context)
        {
            this.context = _context;
        }

        public void Add(UserTakeTool UserTakeTool)
        {
            context.UserTakeTools.Add(UserTakeTool);
        }

        public void Delete(int UserTakeToolID)
        {
            UserTakeTool UserTakeTool = context.UserTakeTools.Find(UserTakeToolID);
            context.UserTakeTools.Remove(UserTakeTool);
        }
        public IEnumerable<UserTakeTool> GetUserTakeTools()
        {
            return context.UserTakeTools.ToList();
        }
        public UserTakeTool GetUserTakeToolByID(int UserTakeToolId)
        {
            return context.UserTakeTools.Find(UserTakeToolId);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(UserTakeTool UserTakeTool)
        {
            context.Entry(UserTakeTool).State = EntityState.Modified;
        }
    }
}
