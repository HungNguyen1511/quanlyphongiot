﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service.Repositories
{
    public interface ICategoryToolRepository
    {
        IEnumerable<CategoryTool> GetCategoryTools();
        CategoryTool GetCategoryToolByID(int categoryToolId);
        void Add(CategoryTool categoryTool);
        void Delete(int categoryToolID);
        void Update(CategoryTool categoryTool);
        void Save();
    }
    public class CategoryToolRepository : ICategoryToolRepository
    {
        private QuanLyThietBiContext context;

        public CategoryToolRepository(QuanLyThietBiContext _context)
        {
            this.context = _context;
        }

        public void Add(CategoryTool categoryTool)
        {
            context.CategoryTools.Add(categoryTool);
        }

        public void Delete(int categoryToolID)
        {
            CategoryTool categoryTool = context.CategoryTools.Find(categoryToolID);
            context.CategoryTools.Remove(categoryTool);
        }
        public IEnumerable<CategoryTool> GetCategoryTools()
        {
            return context.CategoryTools.ToList();
        }

        public CategoryTool GetCategoryToolByID(int categoryToolId)
        {
            return context.CategoryTools.Find(categoryToolId);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(CategoryTool categoryTool)
        {
            context.Entry(categoryTool).State = EntityState.Modified;
        }
    }
}
