﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service.Repositories
{
    public interface ITimelineRepository
    {
        IEnumerable<Timeline> GetTimelines();
        Timeline GetTimelineByID(int timelineId);
        void Add(Timeline Timeline);
        void Delete(int timelineID);
        void Update(Timeline timeline);
        void Save();
    }
    public class TimelineRepository : ITimelineRepository
    {
        private QuanLyThietBiContext context;

        public TimelineRepository(QuanLyThietBiContext _context)
        {
            this.context = _context;
        }

        public void Add(Timeline timeline)
        {
            context.Timelines.Add(timeline);
        }

        public void Delete(int timelineID)
        {
            Timeline timeline = context.Timelines.Find(timelineID);
            context.Timelines.Remove(timeline);
        }
        public IEnumerable<Timeline> GetTimelines()
        {
            return context.Timelines.ToList();
        }

        public Timeline GetTimelineByID(int timelineId)
        {
            return context.Timelines.Find(timelineId);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(Timeline timeline)
        {
            context.Entry(timeline).State = EntityState.Modified;
        }
    }
}
