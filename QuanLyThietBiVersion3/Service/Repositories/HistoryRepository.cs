﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service.Repositories
{
    public interface IHistoryRepository
    {
        IEnumerable<History> GetHistorys();
        History GetHistoryByID(int historyId);
        void Add(History history);
        void Delete(int historyID);
        void Update(History history);
        void Save();
    }
    public class HistoryRepository : IHistoryRepository
    {
        private QuanLyThietBiContext context;

        public HistoryRepository(QuanLyThietBiContext _context)
        {
            this.context = _context;
        }

        public void Add(History history)
        {
            context.Historys.Add(history);
        }

        public void Delete(int historyID)
        {
            History history = context.Historys.Find(historyID);
            context.Historys.Remove(history);
        }
        public IEnumerable<History> GetHistorys()
        {
            return context.Historys.ToList();
        }

        public History GetHistoryByID(int historyId)
        {
            return context.Historys.Find(historyId);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(History history)
        {
            context.Entry(history).State = EntityState.Modified;
        }
    }
}
