﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetUserByID(int UserId);
        void Add(User User);
        void Delete(int UserID);
        void Update(User User);
        void Save();
    }
    public class UserRepository : IUserRepository
    {
        private QuanLyThietBiContext context;

        public UserRepository(QuanLyThietBiContext _context)
        {
            this.context = _context;
        }

        public void Add(User User)
        {
            context.Users.Add(User);
        }

        public void Delete(int UserID)
        {
            User User = context.Users.Find(UserID);
            context.Users.Remove(User);
        }
        public IEnumerable<User> GetUsers()
        {
            return context.Users.ToList();
        }

        public User GetUserByID(int UserId)
        {
            return context.Users.Find(UserId);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(User User)
        {
            context.Entry(User).State = EntityState.Modified;
        }
    }
}
