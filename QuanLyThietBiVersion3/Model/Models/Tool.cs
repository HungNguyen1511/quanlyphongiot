﻿using Model.EntityBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Models
{
    public class Tool : EntityBases
    {
        public int ToolId { set; get; }
        public string ToolName { set; get;}
        public string Description { set; get; }
        public int TotalNumberOfTool { set; get; }
        public int NumberOfLendTool { set; get; }
        public int NumberOfRestTool { set; get; }
        public CategoryTool CategoryTool { set; get; }
        public ICollection<UserTakeTool> UserTakeTools { get; set; }
    }
}
