﻿using Model.EntityBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Models
{
    public class User : EntityBases
    {
        public int UserId { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string CodeCard { set; get; }
        public string Address { set; get; }
        public string PhoneNumber { set; get; }
        public ICollection<UserTakeTool> UserTakeTools { get; set; }
    }
}
