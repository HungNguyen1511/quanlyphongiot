﻿using Model.EntityBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Models
{
    public class CategoryTool : EntityBases
    {
        public int CategoryToolId { set; get; }

        public string CategoryToolName { set; get; }

        public ICollection<Tool> Tools { get; set; }
    }
}
