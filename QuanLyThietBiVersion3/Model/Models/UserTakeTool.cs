﻿using Model.EntityBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Models
{
    public class UserTakeTool: EntityBases
    {
        public int UserId { set; get; }
        public int ToolId { set; get; }
        public Tool Tool { set; get; }
        public User User { set; get; }
    }
}
