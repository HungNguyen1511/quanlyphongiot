﻿using Microsoft.AspNetCore.Mvc;
using Service.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiVersion3.Controllers
{
    [Route("api/[usertaketool-controller]")]
    [ApiController]
    public class UserTakeToolController : ControllerBase
    {
        private IUserTakeToolRepository userTakeToolRepository;

        public UserTakeToolController(IUserTakeToolRepository _userTakeToolRepository)
        {
            this.userTakeToolRepository = _userTakeToolRepository;
        }

    }
    // GET: /Student/
    // GET: /Student/Details/5
    // POST: /Student/Create
    // POST: /Student/Edit/5
    // POST: /Student/Delete/5
}
