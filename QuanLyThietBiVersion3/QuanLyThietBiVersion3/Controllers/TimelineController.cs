﻿using Microsoft.AspNetCore.Mvc;
using Service.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiVersion3.Controllers
{
    [Route("api/[timeline-controller]")]
    [ApiController]
    public class TimelineController : ControllerBase
    {
        private ITimelineRepository historyRepository;

        public TimelineController(ITimelineRepository _historyRepository)
        {
            this.historyRepository = _historyRepository;
        }
        // GET: /Student/
        // GET: /Student/Details/5
        // POST: /Student/Create
        // POST: /Student/Edit/5
        // POST: /Student/Delete/5
    }
}
