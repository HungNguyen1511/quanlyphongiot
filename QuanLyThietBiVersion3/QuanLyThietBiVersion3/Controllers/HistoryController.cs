﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Model.Models;
using QuanLyThietBiVersion3.ViewModel;
using Service.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiVersion3.Controllers
{
    [Route("api/[history-controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        private IHistoryRepository historyRepository;
        private QuanLyThietBiContext context;

        public HistoryController(IHistoryRepository _historyRepository,  QuanLyThietBiContext _context)
        {
            this.historyRepository = _historyRepository;
            this.context = _context;
        }


        [HttpGet]
        [Route("history/")]
        public async Task<ResponeMessageForList<History>> GetAllHistory(int id)
        {
            var list = historyRepository.GetHistorys().ToList();
            return await Task.FromResult(new ResponeMessageForList<History>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = list
            });
        }

        // GET: /CategoryTool/id
        [HttpGet]
        [Route("history/{id}")]
        public async Task<ResponseMessage<History>> GetHistoryById(int id)
        {
            var historyRecord = historyRepository.GetHistoryByID(id);
            return await Task.FromResult(new ResponseMessage<History>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = historyRecord
            });
        }

        // POST: /CategoryTool/Create

        [HttpPost]
        [Route("add-history/{id}")]
        public Task<ResponeMessageForList<string>> AddNewHistoryTool([FromBody] HistoryViewModel historyViewModel)
        {
            context.Historys.Add(new History
            {
                IsDeleted = false,
                CreatedDate = DateTime.Now,
                CreatedBy = 1
            });
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thêm mới thiêt bị thành công"
            });
        }
        // POST: /CategoryTool/Update/id
        [HttpPut]
        [Route("update-history-tool/{id}")]
        public Task<ResponeMessageForList<string>> UpdateHistory([FromBody] HistoryViewModel historyViewModel)
        {
            historyRepository.Update(new History
            {
                IsDeleted = false,
                CreatedDate = DateTime.Now,
                CreatedBy = 1
            });
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công"
            });
        }
        [HttpDelete]
        [Route("delete-category-tool/{id}")]
        // POST: /CategoryTool/Delete/id
        public Task<ResponeMessageForList<string>> DeleteCategoryTool([FromBody] HistoryViewModel historyViewModel)
        {
            historyRepository.Delete(historyViewModel.HistoryId);
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công"
            });
        }
    }
}
