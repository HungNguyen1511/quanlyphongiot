﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Model.Models;
using QuanLyThietBiVersion3.ViewModel;
using Service.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiVersion3.Controllers
{
    [Route("api/[tool-controller]")]
    [ApiController]
    public class ToolController : ControllerBase
    {
        private IToolRepository toolRepository;
        private QuanLyThietBiContext context;

        public ToolController(IToolRepository _toolRepository, QuanLyThietBiContext _context)
        {
            this.toolRepository = _toolRepository;
            this.context = _context;
        }


        [HttpGet]
        [Route("history/")]
        public async Task<ResponeMessageForList<Tool>> GetAllTool(int id)
        {
            var list = toolRepository.GetTools().ToList();
            return await Task.FromResult(new ResponeMessageForList<Tool>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = list
            });
        }

        // GET: /CategoryTool/id
        [HttpGet]
        [Route("history/{id}")]
        public async Task<ResponseMessage<Tool>> GetToolById(int id)
        {
            var toolRecord = toolRepository.GetToolByID(id);
            return await Task.FromResult(new ResponseMessage<Tool>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = toolRecord
            });
        }

        // POST: /CategoryTool/Create

        [HttpPost]
        [Route("add-history/{id}")]
        public Task<ResponeMessageForList<string>> AddNewTool([FromBody] ToolViewModel toolViewModel)
        {
            context.Tools.Add(new Tool
            {
                ToolName = toolViewModel.ToolName,
                NumberOfLendTool = toolViewModel.NumberOfLendTool,
                NumberOfRestTool = toolViewModel.NumberOfRestTool,
                TotalNumberOfTool = toolViewModel.TotalNumberOfTool,
                IsDeleted = false,
                CreatedDate = DateTime.Now,
                CreatedBy = 1
            });
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thêm mới thiêt bị thành công"
            });
        }
        // POST: /CategoryTool/Update/id
        [HttpPut]
        [Route("update-history-tool/{id}")]
        public Task<ResponeMessageForList<string>> UpdateHistory([FromBody] ToolViewModel toolViewModel)
        {
            toolRepository.Update(new Tool
            {
                ToolName = toolViewModel.ToolName,
                NumberOfLendTool = toolViewModel.NumberOfLendTool,
                NumberOfRestTool = toolViewModel.NumberOfRestTool,
                TotalNumberOfTool = toolViewModel.TotalNumberOfTool,
                IsDeleted = false,
                CreatedDate = DateTime.Now,
                CreatedBy = 1
            });
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công"
            });
        }
        [HttpDelete]
        [Route("delete-category-tool/{id}")]
        // POST: /CategoryTool/Delete/id
        public Task<ResponeMessageForList<string>> DeleteTool([FromBody] ToolViewModel toolViewModel)
        {
            toolRepository.Delete(toolViewModel.ToolId);
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công"
            });
        }
    }
}
