﻿using Microsoft.AspNetCore.Mvc;
using Service.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiVersion3.Controllers
{
    [Route("api/[user-controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserRepository userRepository;

        public UserController(IUserRepository _userRepository)
        {
            this.userRepository = _userRepository;
        }
        // GET: /Student/
        // GET: /Student/Details/5
        // POST: /Student/Create
        // POST: /Student/Edit/5
        // POST: /Student/Delete/5
    }
}
