﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Model.Models;
using QuanLyThietBiVersion3.ViewModel;
using Service.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiVersion3.Controllers
{
    [Route("api/[categorytool-controller]")]
    [ApiController]
    public class CategoryToolController : ControllerBase
    {
        private ICategoryToolRepository categoryToolRepository;
        private QuanLyThietBiContext context;
        public CategoryToolController(ICategoryToolRepository _studentRepository, QuanLyThietBiContext _context)
        {
            this.categoryToolRepository = _studentRepository;
            this.context = _context;
        }
        // Get: /CategoryTool

        [HttpGet]
        [Route("category-tool/")]
        public async Task<ResponeMessageForList<CategoryTool>> GetAllCategoryTool(int id)
        {
            var list = categoryToolRepository.GetCategoryTools().ToList();
            return await Task.FromResult(new ResponeMessageForList<CategoryTool>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = list
            });
        }

        // GET: /CategoryTool/id
        [HttpGet]
        [Route("category-tool/{id}")]
        public async Task<ResponseMessage<CategoryTool>> GetCategoryToolId(int id)
        {
            var result = categoryToolRepository.GetCategoryToolByID(id);
            return await Task.FromResult(new ResponseMessage<CategoryTool>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = result
            });
        }

        // POST: /CategoryTool/Create

        [HttpPost]
        [Route("add-category-tool/{id}")]
        public Task<ResponeMessageForList<string>> AddNewCategoryTool([FromBody] CategoryToolViewModel categoryViewModel)
        {
            categoryToolRepository.Add(new CategoryTool
            {
                CategoryToolName = categoryViewModel.CategoryToolName,
                IsDeleted = false,
                CreatedDate = DateTime.Now,
                CreatedBy = 1
            });
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thêm mới thiêt bị thành công"
            });
        }
        // POST: /CategoryTool/Update/id
        [HttpPut]
        [Route("update-category-tool/{id}")]
        public Task<ResponeMessageForList<string>> UpdateCategoryTool([FromBody] CategoryToolViewModel categoryViewModel)
        {
            categoryToolRepository.Update(new CategoryTool
            {
                CategoryToolName = categoryViewModel.CategoryToolName,
                IsDeleted = false,
                CreatedDate = DateTime.Now,
                CreatedBy = 1
            });
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công"
            });
        }
        [HttpDelete]
        [Route("delete-category-tool/{id}")]
        // POST: /CategoryTool/Delete/id
        public Task<ResponeMessageForList<string>> DeleteCategoryTool([FromBody] CategoryToolViewModel categoryToolViewModel)
        {
            categoryToolRepository.Delete(categoryToolViewModel.CategoryToolId);
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công"
            });
        }
    }
}
